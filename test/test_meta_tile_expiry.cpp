//
// Created by adam on 10/23/17.
//

#include "RadosTileService.h"

using namespace radostile;

int main(int argc, char* argv[]) {
    auto service = RadosTileService::GetInstance("raster01");

    std::string pathPrefix = "/radostest";

    Tile tile;
    tile.set_x(2412);
    tile.set_y(3078);
    tile.set_zoom(13);
    tile.set_locale("en");

    if (!service->DeleteMetaTile(pathPrefix, tile)) {
        throw "Failed to delete file";
    }

    size_t byteSize1 = (size_t)(rand() % 500) + 250; // Random number between 250 and 750
    uint8_t tileDataBytes1[byteSize1]; // Random garbage data

    std::vector<uint8_t> tileData1(tileDataBytes1, tileDataBytes1 + byteSize1);

    size_t byteSize2 = (size_t)(rand() % 500) + 250; // Random number between 250 and 750
    uint8_t tileDataBytes2[byteSize2]; // Random garbage data

    std::vector<uint8_t> tileData2(tileDataBytes2, tileDataBytes2 + byteSize2);

    auto currentTime = time(NULL);

    TileWriteParams params;
    params.mutable_tile()->CopyFrom(tile);
    params.set_path_prefix(pathPrefix);
    params.set_data(tileDataBytes1, byteSize1);
    params.set_is_redirect(false);
    params.set_expiry(currentTime);

    if (!service->WriteTile(params)) {
        throw "Failed to write tile";
    }

    printf("Wrote initial tile\n");

    // Write the same tile again with the same expiry.  This write should be rejected
    sleep(3);

    TileWriteParams params2;
    params2.mutable_tile()->CopyFrom(tile);
    params2.set_path_prefix(pathPrefix);
    params2.set_data(tileDataBytes2, byteSize2);
    params2.set_is_redirect(false);
    params2.set_expiry(currentTime);
    service->WriteTile(params2);

    std::vector<uint8_t> outTileData;
    service->ReadTile(pathPrefix, tile, outTileData);

    if (outTileData.size() != byteSize1) {
        throw "Tile should not have been overwritten";
    }

    printf("Wrote again without overwriting\n");

    // Write the file with the updated expiry.  This should delete and overwrite
    sleep(3);
    currentTime = time(NULL);

    TileWriteParams params3;
    params3.mutable_tile()->CopyFrom(tile);
    params3.set_path_prefix(pathPrefix);
    params3.set_data(tileDataBytes2, byteSize2);
    params3.set_is_redirect(false);
    params3.set_expiry(currentTime);
    service->WriteTile(params3);

    service->ReadTile(pathPrefix, tile, outTileData);

    if (outTileData.size() != byteSize2) {
        throw "Failed to overwrite tile";
    }

    printf("Wrote again with overwriting\n");

    printf("All tests passed\n");

    // Cleanup
    service->DeleteMetaTile(pathPrefix, tile);

    return 0;
}