//
// Created by adam on 10/23/17.
//

#include "RadosTileService.h"

using namespace radostile;

int main(int argc, char* argv[]) {
    auto service = RadosTileService::GetInstance("raster01");

    std::string filename = "/radostest/file_test";


    // Delete the meta tile first
    if (!service->DeleteFile(filename)) {
        throw "Failed to delete file";
    }

    printf("Writing file\n");

    size_t byteSize = (size_t)(rand() % 500) + 250; // Random number between 250 and 750
    uint8_t fileDataBytes[byteSize]; // Random garbage data

    std::vector<uint8_t> fileData(fileDataBytes, fileDataBytes + byteSize);
    if (!service->WriteFile(filename, fileData)) {
        throw "Error writing file";
    }

    printf("Finished writing file\n");

    printf("Reading file\n");

    std::vector<uint8_t> outFileData;
    if (!service->ReadFile(filename, outFileData)) {
        throw "Error reading file";
    }

    if (outFileData.size() != fileData.size() ||
            memcmp(outFileData.data(), fileData.data(), outFileData.size()) != 0) {
        throw "Error reading file data";
    }

    printf("Finished reading file\n");

    printf("All tests passed\n");

    // Cleanup
    service->DeleteFile(filename);

    return 0;
}