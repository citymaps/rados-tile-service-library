//
// Created by adam on 10/23/17.
//

#include "RadosTileService.h"

using namespace radostile;

int main(int argc, char* argv[]) {
    auto service = RadosTileService::GetInstance("raster01");

    std::string pathPrefix = "/radostest/v1";
    std::string filename = pathPrefix + "/es/14/2458/3478";

    Tile t;
    t.set_x(2458);
    t.set_y(3478);
    t.set_zoom(14);
    t.set_locale("es");

    std::string pathPrefixTest;
    Tile tileTest;
    service->ParseTilePath(filename, pathPrefixTest, tileTest);

    if (pathPrefixTest != "/radostest/v1") {
        throw "Failed to parse path prefix";
    }

    if (tileTest.SerializeAsString() != t.SerializeAsString()) {
        throw "Failed to parse tile";
    }

    printf("All tests passed\n");

    return 0;
}