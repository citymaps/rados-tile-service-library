//
// Created by adam on 10/23/17.
//

#include "RadosTileService.h"

using namespace radostile;

int main(int argc, char* argv[]) {
    auto service = RadosTileService::GetInstance("raster01");

    std::string pathPrefix = "/radostest";

    Tile tile;
    tile.set_x(2412);
    tile.set_y(3078);
    tile.set_zoom(13);
    tile.set_locale("en");

    if (!service->DeleteMetaTile(pathPrefix, tile)) {
        throw "Failed to delete file";
    }

    if (service->TileExists(pathPrefix, tile)) {
        throw "Tile should not exist yet";
    }

    size_t byteSize1 = (size_t)(rand() % 500) + 250; // Random number between 250 and 750
    uint8_t tileDataBytes1[byteSize1]; // Random garbage data

    std::vector<uint8_t> tileData1(tileDataBytes1, tileDataBytes1 + byteSize1);

    size_t byteSize2 = (size_t)(rand() % 500) + 250; // Random number between 250 and 750
    uint8_t tileDataBytes2[byteSize2]; // Random garbage data

    std::vector<uint8_t> tileData2(tileDataBytes2, tileDataBytes2 + byteSize2);

    auto currentTime = time(NULL);

    TileWriteParams params;
    params.mutable_tile()->CopyFrom(tile);
    params.set_path_prefix(pathPrefix);
    params.set_data(tileDataBytes1, byteSize1);
    params.set_is_redirect(false);
    params.set_expiry(currentTime);

    if (!service->WriteTile(params)) {
        throw "Failed to write tile";
    }

    if (!service->TileExists(pathPrefix, tile)) {
        throw "Tile should exist";
    }

    printf("All tests passed\n");

    // Cleanup
    service->DeleteMetaTile(pathPrefix, tile);

    return 0;
}