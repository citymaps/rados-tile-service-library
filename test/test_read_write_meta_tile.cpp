//
// Created by adam on 10/23/17.
//

#include "RadosTileService.h"

using namespace radostile;

int main(int argc, char* argv[]) {
    auto service = RadosTileService::GetInstance("raster01");

    std::string pathPrefix = "/radostest";

    Tile tile;
    tile.set_x(2408);
    tile.set_y(3072);
    tile.set_zoom(13);
    tile.set_locale("en");

    MetaTile metaTile;
    metaTile.mutable_origin()->CopyFrom(tile);
    metaTile.set_size(8);


    // Delete the meta tile first
    if (!service->DeleteMetaTile(pathPrefix, tile)) {
        throw "Failed to delete file";
    }

    if (service->MetaTileExists(pathPrefix, tile)) {
        throw "Failed to delete meta tile";
    }

    if (service->TileExists(pathPrefix, tile)) {
        throw "Failed to delete meta tile";
    }

    std::vector<uint8_t> data;

    // Write a meta tile
    MetaTileWriteParams params;
    auto& tileInfo = *params.mutable_tile_info();
    size_t offset = 0;

    std::map<uint64_t, TileDescriptor*> tileInfoMap;
    std::hash<Tile> tileHash;

    for (int x = tile.x(); x < tile.x() + metaTile.size(); x++) {
        for (int y = tile.y(); y < tile.y() + metaTile.size(); y++) {
            size_t byteSize = (size_t)(rand() % 500) + 250; // Random number between 250 and 750
            uint8_t tileData[byteSize]; // Random garbage data

            data.insert(data.end(), tileData, tileData + byteSize);

            Tile t;
            t.set_x(x);
            t.set_y(y);
            t.set_zoom(tile.zoom());
            t.set_locale(tile.locale());

            auto desc = tileInfo.Add();
            desc->mutable_tile()->CopyFrom(t);
            desc->set_length(byteSize);
            desc->set_offset(offset);
            desc->set_is_redirect(false);

            auto key = tileHash(t);
            tileInfoMap[key] = desc;

            offset += byteSize;
        }
    }

    params.set_data(data.data(), data.size());
    params.set_path_prefix(pathPrefix);
    params.mutable_meta_tile()->CopyFrom(metaTile);

    printf("Writing meta tile\n");

    if (!service->WriteMetaTile(params)) {
        throw "Failed to write meta tile";
    }

    printf("Finished writing meta tile\n");

    if (!service->MetaTileExists(pathPrefix, tile)) {
        throw "Failed to find if meta tile exists";
    }

    for (int x = tile.x(); x < tile.x() + metaTile.size(); x++) {
        for (int y = tile.y(); y < tile.y() + metaTile.size(); y++) {
            Tile t;
            t.set_x(x);
            t.set_y(y);
            t.set_zoom(tile.zoom());
            t.set_locale(tile.locale());

            if (!service->TileExists(pathPrefix, t)) {
                throw "Failed to find if tile exists";
            }

            auto hash = tileHash(t);
            TileDescriptor* desc = tileInfoMap[hash];

            std::vector<uint8_t> tileData;
            if (!service->ReadTile(pathPrefix, t, tileData)) {
                throw "Failed to read tile";
            }

            printf("Reading tile data %d:%d:%d:%s\n", t.x(), t.y(), t.zoom(), t.locale().c_str());

            auto originalTileData = &data[desc->offset()];
            if (tileData.size() != desc->length() ||
                    memcmp(tileData.data(), originalTileData, desc->length()) != 0) {
                throw "Failed to read tile data correctly";
            }

            printf("Tile data %d:%d:%d:%s read correctly\n", t.x(), t.y(), t.zoom(), t.locale().c_str());
        }
    }

    printf("All tests passed\n");

    // Cleanup
    //service->DeleteMetaTile(pathPrefix, tile);

    return 0;
}