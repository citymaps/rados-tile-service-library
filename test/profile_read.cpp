//
// Created by adam on 10/23/17.
//

#include "RadosTileService.h"

#include <chrono>

using namespace radostile;

int main(int argc, char* argv[]) {
    auto service = RadosTileService::GetInstance("tilecache01");

    struct Bounds {
        int minX, minY, maxX, maxY;
    };

    Bounds tileBounds = {
            2300, 3000,
            2400, 3100
    };

    int zoom = 13;

    double total = 0;

    for (int i = 0; i < 10000; i++) {
        int x = (rand() % (tileBounds.maxX - tileBounds.minX)) + tileBounds.minX;
        int y = (rand() % (tileBounds.maxY - tileBounds.minY)) + tileBounds.minY;

        radostile::Tile t;
        t.set_x(x);
        t.set_y(y);
        t.set_zoom(zoom);
        t.set_locale("global");

        auto startTime = std::chrono::high_resolution_clock::now();

        std::vector<uint8_t> output;
        service->ReadTile("/map/v8", t, output);

        auto endTime = std::chrono::high_resolution_clock::now();

        std::chrono::duration<double, std::milli> elapsed = endTime - startTime;
        total += elapsed.count();

        if (i % 1000 == 0) {
            printf("%d\n", i);
        }
    }

    printf("Average request time: %f\n", total / 10000);


    return 0;
}