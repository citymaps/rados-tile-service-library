//
// Created by adam on 9/21/17.
//

#include <condition_variable>
#include "RadosTileService.h"
#include "Logger.h"
#include "Exception.h"

#include <boost/regex.hpp>
#include <boost/xpressive/xpressive.hpp>

using sregex = boost::xpressive::sregex;
using smatch = boost::xpressive::smatch;

namespace radostile {
    static RadosTileServiceConfiguration sDefaultConfiguration;

    void RadosTileService::Configure(const RadosTileServiceConfiguration &configuration) {
        sDefaultConfiguration = configuration;
    }

    static const int kNumZoomLevels = 25;
    static const std::string kMetaFileName = "metafile";
    static const size_t kHeaderOffset = 0;
    static const size_t kIndexOffset = sizeof(MetaTileHeader);

    static const sregex kTileURLRegex = sregex::compile(
            R"(^/(?P<tileset>[a-z]+)/v(?P<version>\d+)(?P<lang>/[a-z]{2,6})?/(?P<z>\d+)/(?P<x>\d+)/(?P<y>\d+)$)");

    static MetaTileIndexEntry kEmptyEntry = { 0, 0, false };

    RadosTileService::RadosTileService(const std::string &pool) :
            mRadosPool(pool),
            mIndexMemoryCache(kDefaultMemoryCacheSize) {
        // Initialize rados client
        int status;

        mRadosClient.init(sDefaultConfiguration.keyring);
        status = mRadosClient.conf_read_file(sDefaultConfiguration.confFile);
        if (status < 0) {
            throw RadosServiceException("Failed to initialize rados with error: " + std::to_string(status));
        }

        status = mRadosClient.connect();
        if (status < 0) {
            throw RadosServiceException("Failed to connect to rados with error: " + std::to_string(status));
        }

        // Read metadata from rados
        std::vector<uint8_t> metaFileData;
        this->ReadFile(kMetaFileName, metaFileData);
        mMetaTileSizes.resize(kNumZoomLevels);
        memcpy(mMetaTileSizes.data(), metaFileData.data(), metaFileData.size());
    }

    RadosTileService *RadosTileService::GetInstance(const std::string &pool) {
        static std::map<std::string, RadosTileService *> sInstances;
        static std::mutex sInstanceMutex;

        std::lock_guard<std::mutex> lk(sInstanceMutex);

        auto instance = sInstances[pool];
        if (instance == nullptr) {
            instance = new RadosTileService(pool);
            sInstances[pool] = instance;
        }

        return instance;
    }

    bool RadosTileService::ParseTilePath(const std::string &tilePath, std::string &outPathPrefix, Tile &outTile) {
        smatch what;

        try {
            if (boost::xpressive::regex_match(tilePath, what, kTileURLRegex)) {
                outTile.set_x((uint32_t) atoi(what["x"].str().c_str()));
                outTile.set_y((uint32_t) atoi(what["y"].str().c_str()));
                outTile.set_zoom((uint32_t) atoi(what["z"].str().c_str()));

                if (what["lang"]) {
                    outTile.set_locale(what["lang"].str().substr(1));
                }

                outPathPrefix = "/" + what["tileset"].str() + "/v" + what["version"].str();

                return true;
            }
        } catch (...) {
            Logger::Log("Failed to parse tile path: %s", tilePath.c_str());
        }

        return false;
    }

    bool RadosTileService::TileExists(const std::string &path) {
        std::string pathPrefix;
        Tile tile;

        this->ParseTilePath(path, pathPrefix, tile);

        return this->TileExists(pathPrefix, tile);
    }

    bool RadosTileService::TileExists(const std::string &path, const Tile &tile) {
        auto entry = this->ReadEntry(path, tile);
        return entry.length > 0;
    }

    bool RadosTileService::MetaTileExists(const std::string &path, const Tile &tile) {
        MetaTile objectMetaTile;
        this->GetMetaTileForTile(tile, objectMetaTile);
        auto oid = this->GetObjectIdForTile(path, objectMetaTile.origin());

        librados::IoCtx ioCtx;
        mRadosClient.ioctx_create(mRadosPool.c_str(), ioCtx);
        size_t objSize = 0;
        time_t timestamp = 0;
        ioCtx.stat(oid, &objSize, &timestamp);

        return objSize > 0;
    }

    bool RadosTileService::MetaTileExists(const std::string &path) {
        std::string pathPrefix;
        Tile tile;

        this->ParseTilePath(path, pathPrefix, tile);

        return this->MetaTileExists(pathPrefix, tile);
    }

    bool RadosTileService::ReadTile(const std::string &path, const Tile &tile, std::vector<uint8_t> &outputBuffer) {

        MetaTile objectMetaTile;
        this->GetMetaTileForTile(tile, objectMetaTile);
        auto oid = this->GetObjectIdForTile(path, objectMetaTile.origin());

        auto entry = this->ReadEntry(path, tile);
        if (entry.length == 0) {
            outputBuffer.resize(0);
            return false;
        }

        librados::IoCtx ioCtx;
        mRadosClient.ioctx_create(mRadosPool.c_str(), ioCtx);

        outputBuffer.resize(entry.length);
        librados::bufferlist buffer = librados::bufferlist::static_from_mem((char *) outputBuffer.data(),
                                                                            outputBuffer.size());
        int status = ioCtx.read(oid, buffer, entry.length, entry.offset);
        if (status < 0) {
            outputBuffer.resize(0);
            Logger::Log("Failed to read tile with error: %d", status);
            return false;
        }

        if (entry.redirect) {
            size_t size;
            auto redirectId = buffer.to_str();
            status = ioCtx.stat(redirectId, &size, nullptr);
            if (status < 0) {
                outputBuffer.resize(0);
                return false;
            }

            outputBuffer.resize(size);
            buffer = librados::bufferlist::static_from_mem((char *) outputBuffer.data(),
                                                           outputBuffer.size());
            ioCtx.read(redirectId, buffer, size, 0);
        }

        return true;
    }

    bool RadosTileService::ReadTile(const std::string &path, std::vector<uint8_t> &outputBuffer) {
        std::string pathPrefix;
        Tile tile;

        this->ParseTilePath(path, pathPrefix, tile);

        return this->ReadTile(pathPrefix, tile, outputBuffer);
    }

    std::string RadosTileService::GetObjectIdForTile(const std::string &pathPrefix, const Tile &tile) {
        std::stringstream key;
        key << pathPrefix;
        if (tile.locale().length()) {
            key << "/" << tile.locale();
        }
        key << "/" << tile.zoom() << "/" << tile.x() << "/" << tile.y();
        return key.str();
    }


    void RadosTileService::GetMetaTileForTile(const Tile &tile, MetaTile &outTile) {
        outTile.set_size(mMetaTileSizes[tile.zoom()]);

        outTile.mutable_origin()->CopyFrom(tile);

        if (tile.x() % outTile.size() != 0) {
            outTile.mutable_origin()->set_x(tile.x() - (tile.x() % outTile.size()));
        }

        if (tile.y() % outTile.size() != 0) {
            outTile.mutable_origin()->set_y(tile.y() - (tile.y() % outTile.size()));
        }
    }

    bool RadosTileService::WriteMetaTile(const MetaTileWriteParams &params) {
        auto start = std::chrono::high_resolution_clock::now();

        int status;

        // We can write without locks on this thread,
        // since we know the same meta tile cannot be written by multiple threads

        auto &origin = params.meta_tile().origin();
        MetaTile objectMetaTile;
        this->GetMetaTileForTile(origin, objectMetaTile);
        auto oid = this->GetObjectIdForTile(params.path_prefix(), objectMetaTile.origin());

        librados::IoCtx ioCtx;
        mRadosClient.ioctx_create(mRadosPool.c_str(), ioCtx);

        size_t objectCurrentSize = 0;
        time_t t;
        ioCtx.stat(oid, &objectCurrentSize, &t);

        if (objectCurrentSize > 0 && t < params.expiry()) {
            // Delete outdated meta tile
            this->DeleteMetaTile(params.path_prefix(), objectMetaTile.origin());
            objectCurrentSize = 0;
        }

        int tileCount = objectMetaTile.size() * objectMetaTile.size();
        size_t indexSize = tileCount * sizeof(MetaTileIndexEntry);

        MetaTileIndexEntry indexEntries[tileCount];

        int baseObjectOffset;

        if (objectCurrentSize == 0) {
            // This meta tile does not exist, so we need to create the header and the index
            auto currentTime = time(nullptr);

            MetaTileHeader header = {
                    {'M', 'E', 'T', 'A'},
                    (uint32_t) currentTime,
                    (uint32_t) tileCount,
                    (uint32_t) objectMetaTile.origin().zoom(),
                    (uint32_t) objectMetaTile.origin().x(),
                    (uint32_t) objectMetaTile.origin().y()
            };

            memset(indexEntries, 0, indexSize);

            librados::bufferlist metaTileHeaderData;
            metaTileHeaderData.append((const char *) &header, sizeof(MetaTileHeader));
            metaTileHeaderData.append((const char *) indexEntries, indexSize);
            ioCtx.write_full(oid, metaTileHeaderData);
            baseObjectOffset = (int) (kIndexOffset + indexSize);
        } else {
            // Index already exists, pull it down
            auto indexDataBuffer = librados::bufferlist::static_from_mem((char *) indexEntries, indexSize);
            ioCtx.read(oid, indexDataBuffer, indexSize, kIndexOffset);
            baseObjectOffset = (int) objectCurrentSize;
        }

        std::hash<Tile> tileHash;
        // Set up index entries
        int localX = params.meta_tile().origin().x() - objectMetaTile.origin().x();
        int localY = params.meta_tile().origin().y() - objectMetaTile.origin().y();
        size_t numTiles = params.tile_info().size();

        std::map<uint64_t, const TileDescriptor*> tileInfoMap;
        for (auto& tile : params.tile_info()) {
            auto key = tileHash(tile.tile());
            tileInfoMap.insert(std::make_pair(key, &tile));
        }

        for (int y = localY; y < localY + params.meta_tile().size(); y++) {
            for (int x = localX; x < localX + params.meta_tile().size(); x++) {
                Tile tile;
                tile.set_x(objectMetaTile.origin().x() + x);
                tile.set_y(objectMetaTile.origin().y() + y);
                tile.set_zoom(objectMetaTile.origin().zoom());
                tile.set_locale(objectMetaTile.origin().locale());

                auto key = tileHash(tile);
                auto info = tileInfoMap.find(key);
                if (info != tileInfoMap.end()) {
                    int entryIndex = (y * objectMetaTile.size()) + x;
                    auto &entry = indexEntries[entryIndex];

                    if (entry.length == 0 && entry.offset == 0) {
                        // Only make changes to fresh records
                        entry.length = info->second->length();
                        entry.offset = info->second->offset() + baseObjectOffset;
                        entry.redirect = info->second->is_redirect();
                    }
                }
            }
        }

        // Write the tile data
        auto tileDataBuffer = librados::bufferlist::static_from_mem((char *) params.data().data(),
                                                                    params.data().size());
        status = ioCtx.append(oid, tileDataBuffer, params.data().size());
        if (status < 0) {
            Logger::Log("Failed to append body to rados with error: %d", status);
            return false;
        }

        // Write the index entries
        auto outIndexDataBuffer = librados::bufferlist::static_from_mem((char *) indexEntries, indexSize);
        status = ioCtx.write(oid, outIndexDataBuffer, indexSize, kIndexOffset);
        if (status < 0) {
            Logger::Log("Failed to write index to rados with error: %d", status);
            return false;
        }

        auto end = std::chrono::high_resolution_clock::now();
        std::chrono::duration<double, std::milli> duration = end - start;

        auto &tile = params.meta_tile().origin();
        Logger::Log("Finished writing tile %d, %d, %d, %s in %f ms", tile.x(), tile.y(), tile.zoom(),
                    tile.locale().c_str(), duration.count());

        return true;
    }

    bool RadosTileService::DeleteMetaTile(const std::string &pathPrefix, const Tile &tile)
    {
        MetaTile objectMetaTile;
        this->GetMetaTileForTile(tile, objectMetaTile);
        auto oid = this->GetObjectIdForTile(pathPrefix, objectMetaTile.origin());

        return this->DeleteFile(oid);
    }

    bool RadosTileService::WriteTile(const TileWriteParams& params)
    {
        MetaTileWriteParams request;
        request.mutable_meta_tile()->mutable_origin()->CopyFrom(params.tile());
        request.mutable_meta_tile()->set_size(1);

        request.set_expiry(params.expiry());
        request.set_path_prefix(params.path_prefix());

        auto descriptor = request.mutable_tile_info()->Add();
        descriptor->mutable_tile()->CopyFrom(params.tile());
        descriptor->set_offset(0);
        descriptor->set_length(params.data().size());
        descriptor->set_is_redirect(params.is_redirect());

        // Not super proud of this but at the same time, we're not actually going to modify this data, so we're still keeping our const promise
        request.set_allocated_data(const_cast<TileWriteParams&>(params).mutable_data());

        return this->WriteMetaTile(request);
    }

    MetaTileIndexEntry RadosTileService::ReadEntry(const std::string& path, const Tile& tile)
    {
        MetaTile objectMetaTile;
        this->GetMetaTileForTile(tile, objectMetaTile);
        auto oid = this->GetObjectIdForTile(path, objectMetaTile.origin());

        auto localX = tile.x() - objectMetaTile.origin().x();
        auto localY = tile.y() - objectMetaTile.origin().y();
        auto entryIndex = (localY * objectMetaTile.size()) + localX;

        librados::IoCtx ioCtx;
        mRadosClient.ioctx_create(mRadosPool.c_str(), ioCtx);
        size_t objSize = 0;
        time_t timestamp = 0;
        int status = ioCtx.stat(oid, &objSize, &timestamp);
        if (status < 0) {
            if (status != -2) {
                Logger::Log("Failed to stat file, error code: %d", status);
            }
            return kEmptyEntry;
        }

        mMemoryCacheMutex.lock();
        auto cacheEntry = mIndexMemoryCache.get(oid);
        mMemoryCacheMutex.unlock();

        if (cacheEntry && cacheEntry.get()->timestamp >= timestamp) {
            return cacheEntry.get()->index[entryIndex];
        }

        if (objSize > 0) {
            int metaTileSize = mMetaTileSizes[tile.zoom()];
            int tileCount = metaTileSize * metaTileSize;
            size_t indexByteSize = sizeof(MetaTileIndexEntry) * tileCount;
            auto newCacheEntry = std::make_shared<MetaTileIndexCacheEntry>(tileCount, timestamp);
            librados::bufferlist entryBuffer = librados::bufferlist::static_from_mem((char *) newCacheEntry->index, indexByteSize);
            ioCtx.read(oid, entryBuffer, indexByteSize, kIndexOffset);

            auto& entry = newCacheEntry->index[entryIndex];

            mMemoryCacheMutex.lock();
            mIndexMemoryCache.insert(oid, newCacheEntry);
            mMemoryCacheMutex.unlock();

            return entry;
        }

        return kEmptyEntry;
    }

    bool RadosTileService::WriteFile(const std::string &file, const std::vector<uint8_t>& data) {
        librados::IoCtx ioCtx;
        mRadosClient.ioctx_create(mRadosPool.c_str(), ioCtx);

        auto buffer = librados::bufferlist::static_from_mem((char *) data.data(),
                                                            data.size());

        int status = ioCtx.write_full(file, buffer);
        if (status < 0) {
            Logger::Log("Error writing file: %s.  Error code: %d", file.c_str(), status);
            return false;
        }

        return true;
    }

    bool RadosTileService::DeleteFile(const std::string& file)
    {
        librados::IoCtx ioCtx;
        mRadosClient.ioctx_create(mRadosPool.c_str(), ioCtx);

        int status = ioCtx.remove(file);
        if (status < 0 && status != -2) { // We'll allow -2 status code, aka file not found.  No problem deleting nonexistant files
            Logger::Log("Error deleting file: %s.  Error code: %d", file.c_str(), status);
            return false;
        }

        return true;
    }

    bool RadosTileService::ReadFile(const std::string &file, std::vector<uint8_t>& outputBuffer)
    {
        librados::IoCtx ioCtx;
        mRadosClient.ioctx_create(mRadosPool.c_str(), ioCtx);

        size_t size = 0;
        ioCtx.stat(file, &size, nullptr);


        if (size == 0) {
            return false;
        }

        outputBuffer.resize(size);
        auto buffer = librados::bufferlist::static_from_mem((char *) outputBuffer.data(),
                                                            outputBuffer.size());

        int status = ioCtx.read(file, buffer, size, 0);
        if (status < 0) {
            Logger::Log("Error reading file: %s.  Error code: %d", file.c_str(), status);
            return false;
        }

        return true;
    }

    void RadosTileService::SetIndexMemoryCacheSize(size_t memoryCacheSize)
    {
        mMemoryCacheMaxSize = memoryCacheSize;

        mIndexMemoryCache.capacity();
    }

    void RadosTileService::ConstrainMemoryCache()
    {

    }
}