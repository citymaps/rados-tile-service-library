//
// Created by adam on 9/26/17.
//

#pragma once

#include "rados_tile_types.pb.h"

namespace std
{
    template<>
    class hash<radostile::Tile>
    {
    public:
        std::size_t operator()(const radostile::Tile& tile) {
            uint64_t uid = 0;
            uid += ((uint64_t)tile.zoom()) << 58;
            uid += ((uint64_t)tile.x()) << 37;
            uid += ((uint64_t)tile.y()) << 16;
            if (tile.locale().length() == 2) {
                auto localeStr = tile.locale().c_str();
                uid += ((uint32_t)localeStr[0]) << 8;
                uid += localeStr[1];
            }

            return uid;
        }
    };
}
