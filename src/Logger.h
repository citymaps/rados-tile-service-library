//
// Created by adam on 9/25/17.
//

#pragma once

#include <string>

namespace radostile
{
    class Logger
    {
    public:
        template <typename... Args>
        static void Log(const std::string& format, const Args & ... args) {
            try {
#ifdef RADOS_TILE_SERVICE_LOGGING
                printf(format.c_str(), args...);
                printf("\n");
                fflush(stdout);
#endif
            } catch (std::exception e) {
            }
        }
    };
}
