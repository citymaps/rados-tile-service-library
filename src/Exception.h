//
// Created by adam on 10/17/17.
//

#pragma once

#include <stdexcept>

namespace radostile
{
    class RadosServiceException : public std::runtime_error
    {
    public:
        RadosServiceException(const std::string& reason):
                std::runtime_error(reason) {}
    };
}
