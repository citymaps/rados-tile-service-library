//
// Created by adam on 9/21/17.
//

#pragma once

#include "Types.h"

#include <rados/librados.hpp>

#include <async++.h>
#include <memory>
#include <unordered_map>
#include <boost/compute/detail/lru_cache.hpp>

namespace radostile
{
    class BlockingScheduler;

    struct RadosTileServiceConfiguration
    {
        const char* confFile = "/etc/ceph/ceph.conf";
        const char* keyring = "admin";
    };

    static const size_t kDefaultMemoryCacheSize = 12800;

    class RadosTileService
    {
    public:
        ~RadosTileService() = default;

        /**
         * Set the maximum number elements to be stored in the index cache.  0 to turn cache off.  UINT64_MAX for unlimited.
         * @param memoryCacheSize - The new memory cache size.  If the cache is too large, it will be shrunk to the new maximum size
         */
        void SetIndexMemoryCacheSize(size_t memoryCacheSize);

        /**
         * Check if a single tile exists
         *
         * @param pathPrefix - Path prefix for this tile
         * @param tile - The tile to check for
         * @return Whether the tile exists
         */
        bool TileExists(const std::string& pathPrefix, const Tile& tile);

        /**
         * Check if a single tile exists
         *
         * @param path - Fully qualified path for this tile
         * @param tile - The tile to check for
         * @return Whether the tile exists
         */
        bool TileExists(const std::string& path);

        /**
         * Check if a meta tile exists. provided any tile within that meta tile
         *
         * @param pathPrefix - Path prefix for this tile
         * @param tile - The tile to check for
         * @return Whether the meta tile for this tile exists
         */
        bool MetaTileExists(const std::string& pathPrefix, const Tile& tile);

        /**
         * Check if a meta tile exists. provided any tile within that meta tile
         *
         * @param path - Fully qualified path for this tile
         * @param tile - The tile to check for
         * @return Whether the meta tile for this tile exists
         */
        bool MetaTileExists(const std::string& path);

        /**
         * Read a single tile object
         *
         * @param pathPrefix - Path prefix for this tile
         * @param tile - Tile to read
         * @param outputBuffer - A buffer to write data into.  This buffer will be modified.
         * @return True if the read succeeds, false otherwise
         */
        bool ReadTile(const std::string& pathPrefix, const Tile& tile, std::vector<uint8_t>& outputBuffer);

        /**
         * Read a single tile object
         *
         * @param path - Fully qualified path for this tile
         * @param outputBuffer - A buffer to write data into.  This buffer will be modified.
         * @return True if the read succeeds, false otherwise
         */
        bool ReadTile(const std::string& path, std::vector<uint8_t>& outputBuffer);

        /**
         * Write an NxN grid of tiles
         *
         * @param params - Parameters for this write request
         * @return True if the write succeeds, false otherwise
         */
        bool WriteMetaTile(const MetaTileWriteParams& params);

        /**
         * Delete a meta tile
         *
         * @param pathPrefix - Path prefix for this tile
         * @param tile - Tile to delete.  This can be any tile within the NxN grid of this meta tile
         * @return True if the write succeeds, false otherwise
         */
        bool DeleteMetaTile(const std::string& pathPrefix, const Tile& tile);

        /**
         * Write a single tile
         *
         * @param params - Parameters for this write request
         * @return True if the write succeeds, false otherwise
         */
        bool WriteTile(const TileWriteParams& params);

        /**
         * Read a non-tile file from the pool
         * @param file - The path of this file
         * @param outputBuffer - A buffer to write data into.  This buffer will be modified.
         * @return True if the read succeeds, false otherwise
         */
        bool ReadFile(const std::string& file, std::vector<uint8_t>& outputBuffer);

        /**
         * Write a non-tile file into the pool
         * @param file - The path of this file
         * @param data - The data of this file
         * @return True if the write succeeds, false otherwise
         */
        bool WriteFile(const std::string& file, const std::vector<uint8_t>& data);

        /**
         * Delete an object id from the pool
         * @param file - The object ID to delete
         * @return - True if the delete succeeds
         */
        bool DeleteFile(const std::string& file);

        /**
         * Extract tile object and path prefix from a fully qualified tile path
         *
         * @param tilePath - The fully qualified tile path
         * @param outPathPrefix - The output of the path prefix
         * @param outTile - The output of the tile
         * @return True if the path could be parsed, false otherwise
         */
        bool ParseTilePath(const std::string& tilePath, std::string& outPathPrefix, Tile& outTile);

        /**
         * Get the root meta tile for an individual tile
         * @param tile - An individual tile
         * @param outTile - The meta tile object owning this tile
         */
        void GetMetaTileForTile(const Tile& tile, MetaTile& outTile);

        /**
         * Override the default ceph configurations
         * @param configuration - Ceph configuration parameters
         */
        static void Configure(const RadosTileServiceConfiguration& configuration);

        /**
         * Get the RadosTileService for the given pool.  Each service is a singleton.  This object can be
         * considered "thread safe".  It can be used on any number of threads without worry.
         *
         * @param pool - The pool associated with this object
         * @return A Rados tile service object for the given pool
         */
        static RadosTileService* GetInstance(const std::string& pool);

    private:
        explicit RadosTileService(const std::string& pool);

        librados::Rados mRadosClient;
        std::string mRadosPool;

        std::vector<uint32_t> mMetaTileSizes;

        boost::compute::detail::lru_cache<std::string, std::shared_ptr<MetaTileIndexCacheEntry>> mIndexMemoryCache;
        size_t mMemoryCacheMaxSize = kDefaultMemoryCacheSize;
        std::mutex mMemoryCacheMutex;

        std::string GetObjectIdForTile(const std::string& pathPrefix, const Tile& tile);
        MetaTileIndexEntry ReadEntry(const std::string& path, const Tile& tile);
        void ConstrainMemoryCache();
    };
}


