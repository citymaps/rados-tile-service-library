//
// Created by adam on 10/17/17.
//

#pragma once

#include "rados_tile_types.pb.h"
#include "rados_tile_types_hash.h"

namespace radostile
{
    struct MetaTileHeader
    {
        char signature[4];
        uint32_t time;
        uint32_t tileCount;
        uint32_t zoom;
        uint32_t x;
        uint32_t y;
    };

    struct MetaTileIndexEntry
    {
        uint32_t offset;
        uint32_t length : 24;
        bool redirect;
    };

    static_assert(sizeof(MetaTileIndexEntry) == 8);

    struct MetaTileIndexCacheEntry
    {
        MetaTileIndexEntry* index;
        time_t timestamp;

        MetaTileIndexCacheEntry(int tileCount, time_t timestamp_):
                timestamp(timestamp_), index(new MetaTileIndexEntry[tileCount])
        {
            memset(index, 0, sizeof(MetaTileIndexEntry) * tileCount);
        }

        ~MetaTileIndexCacheEntry()
        {
            delete index;
        }
    };
}
